package com.bellal.json_parsing.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.bellal.json_parsing.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static String url = "http://api.androidhive.info/contacts/";
    private TextView textView;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.display);

        new JTask().execute(url);


    }


    class JTask extends AsyncTask<String, Void, String> {

        List lst = new ArrayList();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            //HttpHandler sh = new HttpHandler();
            String response = null;
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");

                InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder builder = new StringBuilder();
                String inputString;
                while ((inputString = bufferedReader.readLine()) != null) {
                    builder.append(inputString);
                }
                JSONObject jsonObject = new JSONObject(builder.toString());
                JSONArray contacts = jsonObject.getJSONArray("contacts");

                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject jObject = contacts.getJSONObject(i);

                    //String id = jObject.getString("id");
                    response = jsonObject.toString();
                }
                urlConnection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }


            return response;
        }

        @Override
        protected void onPostExecute(String contacts) {
            super.onPostExecute(contacts);

            if (pDialog.isShowing())
                pDialog.dismiss();
            textView.setText(contacts);
        }
    }
}
